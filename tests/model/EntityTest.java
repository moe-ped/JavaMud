package model;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.component.Hitpoints;
import model.component.Type;

class EntityTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testGetComponent() {
		Entity entity = new Entity();
		entity.addComponent(new Hitpoints());
		entity.getComponent(Hitpoints.class);
		assertNotNull(entity.getComponent(Hitpoints.class));
		assertNull(entity.getComponent(Type.class));
	}

}
