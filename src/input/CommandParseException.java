package input;

public class CommandParseException extends Exception {

	private static final long serialVersionUID = -7284834414291112985L;

	public CommandParseException(String message) {
		super(message);
	}

}
