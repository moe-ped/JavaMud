package input;

import java.time.LocalTime;

import message.ActionMsg;
import message.AttackEntityMsg;
import message.LookMsg;
import model.Entity;
import model.GameModel;

public class CommandParser {

	private GameModel gameModel;

	public CommandParser(GameModel gameModel) {
		this.gameModel = gameModel;
	}

	public ActionMsg parse(String phrase) throws CommandParseException {
		// Test
		String[] parts = phrase.split(" ");
		String command = parts[0];
		switch (command) {
		case "attack":
			return parseAttack(parts);
		case "look":
			return parseLook(parts);
		default:
			throw (new CommandParseException("I don't know how to " + command));
		}
	}

	private AttackEntityMsg parseAttack(String[] params) throws CommandParseException {
		String name = params[1];
		Entity entity = gameModel.getEntityByName(name);
		if (entity == null) {
			entity = gameModel.getEntityByType(name);
		}
		if (entity == null) {
			throw (new CommandParseException("I don't see a " + name + " here."));
		}
		return new AttackEntityMsg(LocalTime.now(), gameModel.getOwnPlayerId(), entity.getId());
	}

	private LookMsg parseLook(String[] parts) {
		return new LookMsg(LocalTime.now());
	}

}
