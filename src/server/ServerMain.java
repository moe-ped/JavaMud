package server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ServerMain {

	private Logger log = LogManager.getLogger(getClass());

	private Server server;

	public static void main(String[] args) {
		ServerMain main = new ServerMain();
		main.init();
	}

	public void init() {
		server = new Server();
		log.info("serverStartAction");
		server.start();
	}

}
