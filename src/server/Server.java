package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import message.ActionMsg;
import message.AttackEntityMsg;
import message.EntityDiedMsg;
import message.EntityHealthChangedMsg;
import message.GameModelMsg;
import message.Message;
import model.Entity;
import model.GameModel;
import model.component.Aggressor;
import model.component.Damage;
import model.component.Hitpoints;
import model.component.Name;
import model.component.Type;
import system.CombatSystem;

public class Server {

	private Logger log = LogManager.getLogger(getClass());

	private ServerSocket serverSocket;

	private List<Socket> clientSockets = new ArrayList<Socket>();
	private List<ObjectInputStream> inputStreams = new ArrayList<ObjectInputStream>();
	private List<ObjectOutputStream> outputStreams = new ArrayList<ObjectOutputStream>();

	private boolean running = false;

	private GameModel gameModel;

	private CombatSystem combatSystem;

	public Server() {
	}

	public void start() {
		running = true;
		run();
	}

	public void stop() {
		running = false;
	}

	private void run() {
		try {
			serverSocket = new ServerSocket(1234);
			loadGameModel();
			combatSystem = new CombatSystem(gameModel);
			subscribeEvents();
			new Thread(() -> waitForClient()).start();
			new Thread(() -> updateSystems()).start();
		} catch (IOException e) {
			log.error(e.fillInStackTrace());
		}
	}

	private void subscribeEvents() {
		gameModel.entityHealthChanged.add(this::onEntityHealthChanged);
		gameModel.entityDied.add(this::onEntityDied);

		gameModel.entityAdded.add(combatSystem::onEntityAdded);
		gameModel.entityDied.add(combatSystem::onEntityRemoved);
		gameModel.entityRemoved.add(combatSystem::onEntityRemoved);
	}

	private void updateSystems() {
		while (running) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				log.error(e.fillInStackTrace());
			}
			combatSystem.update();
		}
	}

	private void waitForClient() {
		try {
			while (running) {
				log.debug("Waiting for client ...");
				Socket clientSocket = serverSocket.accept();
				clientSockets.add(clientSocket);
				ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());
				inputStreams.add(in);
				ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());
				outputStreams.add(out);

				GameModelMsg msg = new GameModelMsg(LocalTime.now(), gameModel);
				out.writeObject(msg);

				waitForMessage(in);
			}
		} catch (ClassNotFoundException | IOException e) {
			log.error(e.fillInStackTrace());
		}
	}

	private void waitForMessage(ObjectInputStream in) throws IOException, ClassNotFoundException {
		// TODO: Handle disconnect
		while (running) {
			ActionMsg msg = (ActionMsg) in.readObject();
			log.debug("Message received: " + msg);
			handleMessage(msg);
		}
	}

	// region Events

	private void onEntityHealthChanged(int id, float oldHealth, float newHealth) {
		EntityHealthChangedMsg responseMsg = new EntityHealthChangedMsg(LocalTime.now(), id, oldHealth, newHealth);
		notifyAll(responseMsg);
	}

	private void onEntityDied(int id) {
		// NOTE: Message is useless
		EntityDiedMsg responseMsg = new EntityDiedMsg(LocalTime.now(), id);
		notifyAll(responseMsg);
	}

	// endregion

	private void loadGameModel() {
		// TODO: Save to/ load from json
		gameModel = new GameModel();

		Entity player = new Entity();
		player.setId(0);
		Hitpoints hpComponent = new Hitpoints();
		hpComponent.setHitpoints(100.0f);
		Name nameComponent = new Name();
		nameComponent.setName("hero");
		Type typeComponent = new Type();
		typeComponent.setType("rogue");
		Damage damageComponent = new Damage();
		damageComponent.setDamage(40.0f);
		Aggressor aggressorComponent = new Aggressor();
		player.addComponents(hpComponent, nameComponent, typeComponent, damageComponent, aggressorComponent);
		gameModel.addEntity(player);

		Entity enemy1 = new Entity();
		enemy1.setId(1);
		hpComponent = new Hitpoints();
		hpComponent.setHitpoints(60.0f);
		nameComponent = new Name();
		nameComponent.setName("thrall");
		typeComponent = new Type();
		typeComponent.setType("orc");
		damageComponent = new Damage();
		damageComponent.setDamage(20.0f);
		aggressorComponent = new Aggressor();
		enemy1.addComponents(hpComponent, nameComponent, typeComponent, damageComponent, aggressorComponent);
		gameModel.addEntity(enemy1);

		Entity enemy2 = new Entity();
		enemy2.setId(2);
		hpComponent = new Hitpoints();
		hpComponent.setHitpoints(60.0f);
		nameComponent = new Name();
		nameComponent.setName("lurtz");
		typeComponent = new Type();
		typeComponent.setType("orc");
		damageComponent = new Damage();
		damageComponent.setDamage(20.0f);
		aggressorComponent = new Aggressor();
		enemy2.addComponents(hpComponent, nameComponent, typeComponent, damageComponent, aggressorComponent);
		gameModel.addEntity(enemy2);

		Entity chair = new Entity();
		chair.setId(3);
		hpComponent = new Hitpoints();
		hpComponent.setHitpoints(500.0f);
		typeComponent = new Type();
		typeComponent.setType("chair");
		chair.addComponents(hpComponent, typeComponent);
		gameModel.addEntity(chair);
	}

	private void handleMessage(ActionMsg msg) throws IOException {
		if (msg instanceof AttackEntityMsg) {
			AttackEntityMsg aMsg = (AttackEntityMsg) msg;
			handleAttackEntityMsg(aMsg);
		} else {
			log.error("Unknown message type: " + msg.getClass().getName());
		}
	}

	private void handleAttackEntityMsg(AttackEntityMsg msg) throws IOException {
		combatSystem.setTarget(msg.getSourceId(), msg.getTargetId());
	}

	private void notifyAll(Message msg) {
		for (ObjectOutputStream outputStream : outputStreams) {
			try {
				outputStream.writeObject(msg);
			} catch (IOException e) {
				log.error(e.fillInStackTrace());
			}
		}
	}

}
