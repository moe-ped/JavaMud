package ui;

import java.io.IOException;
import java.net.URL;
import java.time.LocalTime;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import client.Client;
import input.CommandParseException;
import input.CommandParser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import message.LookMsg;
import message.Message;
import model.Entity;
import model.GameModel;
import util.EntityUtils;
import util.StringUtils;

public class SampleController implements Initializable {

	@FXML private TextField commandline;
	@FXML private TextArea logTextField;

	private Logger log = LogManager.getLogger(getClass());

	private Client client;
	private CommandParser commandParser;

	private GameModel gameModel;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		client = new Client();

		Thread thread = new Thread(() -> connectToServerAction(new ActionEvent()));
		thread.start();

		while (client.getGameModel() == null) {
			Thread.yield();
		}

		gameModel = client.getGameModel();
		log.debug(gameModel);
		commandParser = new CommandParser(gameModel);
		setupEvents();
	}

	// TODO: Hotkey handler
	private void setupEvents() {
		commandline.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				onCommandEntered(null);
			}
		});
		gameModel.entityHealthChanged.add(this::onEntityHealthChanged);
	}

	// region Events

	void onEntityHealthChanged(int id, float oldHealth, float newHealth) {
		Entity entity = gameModel.getEntityById(id);
		writeToLog(StringUtils.capitalize(EntityUtils.getSimpleName(entity)) + " health: " + newHealth);
	}

	// endregion

	// region FX Events

	@FXML
	void onCommandEntered(ActionEvent event) {
		try {
			log.info("messageSendAction");
			LocalTime timestamp = LocalTime.now();
			commandParser = new CommandParser(client.getGameModel());

			Message msg = commandParser.parse(commandline.getText().toLowerCase());
			msg.setTime(timestamp);
			if (msg instanceof LookMsg) {
				writeToLog(getLookText());
			} else {
				client.writeMsg(msg);
			}
		} catch (IOException e) {
			log.error(e.fillInStackTrace());
		} catch (CommandParseException e) {
			log.error(e.fillInStackTrace());
		}
		commandline.setText("");
	}

	@FXML
	public void connectToServerAction(ActionEvent event) {
		log.info("connectToServerAction");
		client.connect("127.0.0.2");
	}

	// endregion

	private void writeToLog(String msg) {
		logTextField.appendText(msg);
		logTextField.appendText(System.getProperty("line.separator"));
	}

	private String getLookText() {
		StringBuilder sb = new StringBuilder();
		sb.append("You see here: ");
		List<Entity> entities = gameModel.getEntities();
		int max = entities.size();
		for (int i = 0; i < max; i++) {
			sb.append(EntityUtils.getFullName(entities.get(i)));
			if (i == max - 2) {
				sb.append(" and ");
			} else if (i < max - 2) {
				sb.append(", ");
			}
		}
		sb.append(".");
		return sb.toString();
	}

}
