package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.concurrent.Task;
import message.EntityDiedMsg;
import message.EntityHealthChangedMsg;
import message.GameModelMsg;
import message.Message;
import model.GameModel;
import model.component.Hitpoints;

public class Client {

	private Logger log = LogManager.getLogger(getClass());

	private ObjectOutputStream out;
	private ObjectInputStream in;
	private String serverhost;
	private Socket serverSocket;
	private GameModel gameModel;

	public String getServerhost() {
		return serverhost;
	}

	public void setServerhost(String serverhost) {
		this.serverhost = serverhost;
	}

	public GameModel getGameModel() {
		return gameModel;
	}

	public boolean connect(String remoteHost) {
		log.info(remoteHost);
		try {
			serverSocket = new Socket("localhost", 1234);
			out = new ObjectOutputStream(serverSocket.getOutputStream());
			in = new ObjectInputStream(serverSocket.getInputStream());
			waitForMessage();
		} catch (IOException | ClassNotFoundException e) {
			log.error("Server connect");
		}
		return serverSocket.isConnected();
	}

	public void writeMsg(Message msg) throws IOException {
		log.info("Sending message: " + msg);
		out.writeObject(msg);
	}

	private void waitForMessage() throws IOException, ClassNotFoundException {
		// TODO: Handle disconnect
		Task<String> task = new Task<String>() {

			@Override
			protected String call() throws Exception {
				while (true) {
					Message msg = (Message) in.readObject();
					log.info("Message received: " + msg);
					handleMessage(msg);
				}
			}
		};
		Thread thread = new Thread(task);
		thread.start();
	}

	private void handleMessage(Message msg) {
		if (msg instanceof GameModelMsg) {
			GameModelMsg gMsg = (GameModelMsg) msg;
			gameModel = gMsg.getGameModel();

			// TODO: Accounts
			gameModel.setOwnPlayerId(0);
		} else if (msg instanceof EntityHealthChangedMsg) {
			EntityHealthChangedMsg eMsg = (EntityHealthChangedMsg) msg;
			gameModel.getEntityById(eMsg.getEntityId()).getComponent(Hitpoints.class)
					.setHitpoints(eMsg.getNewValue());
		} else if (msg instanceof EntityDiedMsg) {
			// Enemy is already dead
		} else {
			log.error("Unknown message type: " + msg.getClass().getName());
		}
	}

}
