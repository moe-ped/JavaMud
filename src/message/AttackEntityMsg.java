package message;

import java.time.LocalTime;

public class AttackEntityMsg extends ActionMsg {

	private static final long serialVersionUID = -1446132858381660103L;

	private int playerId;
	private int enemyId;

	public int getSourceId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public int getTargetId() {
		return enemyId;
	}

	public void setEnemyId(int enemyId) {
		this.enemyId = enemyId;
	}

	public AttackEntityMsg(LocalTime time, int playerId, int enemyId) {
		super(time);
		this.setPlayerId(playerId);
		this.setEnemyId(enemyId);
	}

}
