package message;

import java.time.LocalTime;

import model.GameModel;

public class GameModelMsg extends Message {

	private static final long serialVersionUID = 4994334040331465876L;

	private GameModel gameModel;

	public GameModel getGameModel() {
		return gameModel;
	}

	public void setGameModel(GameModel gameModel) {
		this.gameModel = gameModel;
	}

	public GameModelMsg(LocalTime time, GameModel gameModel) {
		super(time);
		this.gameModel = gameModel;
	}

}
