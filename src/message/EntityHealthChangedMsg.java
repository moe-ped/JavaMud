package message;

import java.time.LocalTime;

public class EntityHealthChangedMsg extends ValueChangedMsg<Float> {

	private static final long serialVersionUID = 5909900463730116922L;

	public EntityHealthChangedMsg(LocalTime time, int objectId, Float oldValue, Float newValue) {
		super(time, objectId, oldValue, newValue);
	}

}
