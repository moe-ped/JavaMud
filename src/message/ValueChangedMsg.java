package message;

import java.time.LocalTime;

@SuppressWarnings("serial")
public abstract class ValueChangedMsg<T> extends Message {

	// Object whose value has changed, e.g. player id
	private int objectId;
	private T oldValue;
	private T newValue;

	public int getEntityId() {
		return objectId;
	}

	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}

	public T getOldValue() {
		return oldValue;
	}

	public void setOldValue(T oldValue) {
		this.oldValue = oldValue;
	}

	public T getNewValue() {
		return newValue;
	}

	public void setNewValue(T newValue) {
		this.newValue = newValue;
	}

	public ValueChangedMsg(LocalTime time, int objectId, T oldValue, T newValue) {
		super(time);
		this.setObjectId(objectId);
		this.setOldValue(oldValue);
		this.setNewValue(newValue);
	}

}
