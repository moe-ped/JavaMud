package message;

import java.time.LocalTime;

public class EntityDiedMsg extends Message {

	private static final long serialVersionUID = 5909900463730116922L;

	private int objectId;

	public int getObjectId() {
		return objectId;
	}

	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}

	public EntityDiedMsg(LocalTime time, int objectId) {
		super(time);
		this.objectId = objectId;
	}

}
