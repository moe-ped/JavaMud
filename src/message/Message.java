package message;

import java.io.Serializable;
import java.time.LocalTime;

@SuppressWarnings("serial")
public abstract class Message implements Serializable {

	protected LocalTime time;

	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}

	public Message(LocalTime time) {
		super();
		this.time = time;
	}

}
