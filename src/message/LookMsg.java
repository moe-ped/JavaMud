package message;

import java.time.LocalTime;

public class LookMsg extends ActionMsg {

	private static final long serialVersionUID = 6518086893728606284L;

	public LookMsg(LocalTime time) {
		super(time);
	}

}
