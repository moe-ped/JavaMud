package message;

import java.time.LocalTime;

@SuppressWarnings("serial")
public abstract class ActionMsg extends Message {

	public ActionMsg(LocalTime time) {
		super(time);
	}
}
