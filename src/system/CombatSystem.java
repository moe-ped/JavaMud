package system;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import model.Entity;
import model.GameModel;
import model.component.Aggressor;
import model.component.Damage;
import model.component.Hitpoints;

public class CombatSystem {

	private Logger log = LogManager.getLogger(getClass());
	private GameModel gameModel;

	private List<Entity> aggressors;

	public CombatSystem(GameModel gameModel) {
		this.gameModel = gameModel;
		aggressors = new ArrayList<Entity>();
		addIfAggressor(gameModel.getEntities());
	}

	public void update() {
		try {
			for (Entity aggressor : aggressors) {
				Aggressor aggressorComponent = aggressor.getComponent(Aggressor.class);
				int victimId = aggressorComponent.getTargetId();
				if (victimId < 0) {
					continue;
				}
				Entity victim = gameModel.getEntityById(victimId);

				float oldHp = victim.getComponent(Hitpoints.class).getHitpoints();
				float damage = aggressor.getComponent(Damage.class).getDamage();
				float newHp = oldHp - damage;

				victim.getComponent(Hitpoints.class).setHitpoints(newHp);
			}
		} catch (ConcurrentModificationException e) {
			// FIXME: HACK
			update();
		}
	}

	public void setTarget(int aggressorId, int victimId) {
		Entity aggressor = gameModel.getEntityById(aggressorId);
		Entity victim = gameModel.getEntityById(victimId);
		Aggressor aggressorComponent = aggressor.getComponent(Aggressor.class);
		aggressorComponent.setTargetId(victimId);
		aggressorComponent = victim.getComponent(Aggressor.class);
		if (aggressorComponent != null) {
			aggressorComponent.setTargetId(aggressorId);
		}
	}

	// region Events

	public void onEntityAdded(int id) {
		Entity aggressor = gameModel.getEntityById(id);
		addIfAggressor(aggressor);
	}

	public void onEntityRemoved(int id) {
		log.debug("Entity removed");
		Entity aggressor = gameModel.getEntityById(id);
		aggressors.remove(aggressor);
		for (Entity entity : gameModel.getEntities()) {
			Aggressor aggressorComponent = entity.getComponent(Aggressor.class);
			if (aggressorComponent != null && aggressorComponent.getTargetId() == id) {
				aggressorComponent.setTargetId(-1);
			}
		}
	}

	// endregion

	private void addIfAggressor(List<Entity> entities) {
		for (Entity entity : entities) {
			addIfAggressor(entity);
		}
	}

	private void addIfAggressor(Entity entity) {
		if (entity.hasComponent(Aggressor.class)) {
			aggressors.add(entity);
		}
	}

}
