package model.component;

public class Hitpoints extends Component {

	private static final long serialVersionUID = 2554872101833631032L;

	private float hitpoints;

	public float getHitpoints() {
		return hitpoints;
	}

	public void setHitpoints(float hitpoints) {
		this.hitpoints = hitpoints;
		valueChanged.call(this);
	}

}
