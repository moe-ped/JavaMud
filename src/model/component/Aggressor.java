package model.component;

public class Aggressor extends Component {

	private static final long serialVersionUID = -5036310186063927875L;

	private int targetId = -1;

	public int getTargetId() {
		return targetId;
	}

	public void setTargetId(int targetId) {
		this.targetId = targetId;
		valueChanged.call(this);
	}

}
