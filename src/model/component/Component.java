package model.component;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

import util.action.Action;

@SuppressWarnings("serial")
public abstract class Component implements Serializable {

	public transient Action<Component> valueChanged = new Action<Component>();

	// The id of the parent entity
	private int parentId;

	public Component() {
		valueChanged = new Action<Component>();
	}

	// Gets called for deserialization
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		valueChanged = new Action<Component>();
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

}
