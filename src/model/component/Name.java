package model.component;

public class Name extends Component {

	private static final long serialVersionUID = 8347788255860172839L;

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		valueChanged.call(this);
	}

}
