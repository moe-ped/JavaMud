package model.component;

public class Damage extends Component {

	private static final long serialVersionUID = -6898529970367457905L;

	private float damage;

	// TODO: Calculate damage
	public float getDamage() {
		return damage;
	}

	public void setDamage(float hitpoints) {
		this.damage = hitpoints;
		valueChanged.call(this);
	}

}
