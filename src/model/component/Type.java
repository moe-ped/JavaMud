package model.component;

public class Type extends Component {

	private static final long serialVersionUID = -2880299003322943901L;

	/**
	 * e.g. "orc", "paladin", "sword", "chair" <br>
	 * Might as well be called race, but includes inanimate object, so type it is
	 */
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
		valueChanged.call(this);
	}

}
