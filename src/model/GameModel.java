package model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import model.component.Component;
import model.component.Hitpoints;
import model.component.Name;
import model.component.Type;
import util.action.Action;
import util.action.Action3;

public class GameModel implements Serializable {

	private static final long serialVersionUID = 1424620691476285354L;

	private Logger log = LogManager.getLogger(getClass());

	public transient Action<Integer> entityAdded;
	public transient Action<Integer> entityRemoved;
	public transient Action3<Integer, Float, Float> entityHealthChanged;
	public transient Action<Integer> entityDied;

	private List<Entity> entities = new ArrayList<Entity>();

	private int ownPlayerId = -1;

	public GameModel() {
		entityAdded = new Action<Integer>();
		entityRemoved = new Action<Integer>();
		entityHealthChanged = new Action3<Integer, Float, Float>();
		entityDied = new Action<Integer>();
		subscribeEvents();
	}

	// Gets called for deserialization
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		entityAdded = new Action<Integer>();
		entityRemoved = new Action<Integer>();
		entityHealthChanged = new Action3<Integer, Float, Float>();
		entityDied = new Action<Integer>();
		subscribeEvents();
	}

	private void subscribeEvents() {
		for (Entity entity : entities) {
			entity.componentChanged.add(this::onEntityChanged);
		}
	}

	private void onEntityChanged(Component component) {
		if (component instanceof Hitpoints) {
			Hitpoints hpComponent = (Hitpoints) component;
			int id = hpComponent.getParentId();
			float hp = hpComponent.getHitpoints();
			entityHealthChanged.call(id, -1.0f, hp);
			if (hpComponent.getHitpoints() <= 0) {
				entityDied.call(id);
				entities.remove(getEntityById(id));
			}
		}
	}

	/**
	 * Client side only
	 * 
	 * @return
	 */
	public int getOwnPlayerId() {
		return ownPlayerId;
	}

	/**
	 * Client side only
	 * 
	 * @return
	 */
	public void setOwnPlayerId(int ownPlayerId) {
		this.ownPlayerId = ownPlayerId;
	}

	public void addEntity(Entity entity) {
		entities.add(entity);
		entityAdded.call(entity.getId());
		entity.componentChanged.add(this::onEntityChanged);
	}

	public List<Entity> getEntities() {
		return entities;
	}

	/**
	 * @param name
	 *            e.g. "lurtz"
	 * @return: the first entity of that name, null if none exists
	 */
	public Entity getEntityByName(String name) {
		try {
			Entity entity = entities.stream()
					.filter(e -> e.hasComponent(Name.class) && e.getComponent(Name.class).getName().equals(name))
					.findFirst().get();
			return entity;
		} catch (Exception e) {
			log.warn("No entity called " + name + " found.");
		}
		return null;
	}

	/**
	 * @param type
	 *            e.g. "orc" or "goblin"
	 * @return: the first entity of that type, null if none exists
	 */
	public Entity getEntityByType(String type) {
		try {
			Entity entity = entities.stream()
					.filter(e -> e.hasComponent(Type.class) && e.getComponent(Type.class).getType().equals(type))
					.findFirst().get();
			return entity;
		} catch (Exception e) {
			log.warn("No entity of type " + type + " found.");
		}
		return null;
	}

	/**
	 * @param id
	 * @return: the first entity with that id, null if none exists
	 */
	public Entity getEntityById(int id) {
		try {
			Entity entity = entities.stream()
					.filter(e -> e.getId() == id)
					.findFirst().get();
			return entity;
		} catch (Exception e) {
			log.warn("No entity with id " + id + " found.");
		}
		return null;
	}

}
