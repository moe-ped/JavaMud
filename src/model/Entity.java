package model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import model.component.Component;
import util.action.Action;

public final class Entity implements Serializable {

	private static final long serialVersionUID = -8177605505200545692L;

	private Logger log = LogManager.getLogger(getClass());

	private int id;
	private List<Component> components;

	public transient Action<Component> componentChanged;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Entity() {
		components = new ArrayList<Component>();
		componentChanged = new Action<Component>();
		subscribeEvents();
	}

	// Gets called for deserialization
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		componentChanged = new Action<Component>();
		subscribeEvents();
	}

	private void subscribeEvents() {
		for (Component component : components) {
			component.valueChanged.add(c -> componentChanged.call(c));
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends Component> T getComponent(Class<T> type) {
		try {
			return (T) components.stream().filter(c -> type.isInstance(c)).findFirst().get();
		} catch (NoSuchElementException | NullPointerException e) {
			log.warn("Entity " + id + " does not have a component of type " + type.getName());
		}
		return null;
	}

	public <T extends Component> boolean hasComponent(Class<T> type) {
		return components.stream().anyMatch(c -> type.isInstance(c));
	}

	public void addComponents(Component... components) {
		for (Component component : components) {
			addComponent(component);
			component.setParentId(id);
			component.valueChanged.add(c -> componentChanged.call(c));
		}
	}

	public void addComponent(Component component) {
		components.add(component);
	}

}
