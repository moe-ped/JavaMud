package util.action;

@FunctionalInterface
public interface ActionElement3<T1, T2, T3> {
	void call(T1 p1, T2 p2, T3 p3);
}
