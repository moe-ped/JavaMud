package util.action;

import java.util.ArrayList;
import java.util.List;

// TODO: Is there a generic solution for this?
/**
 * Equivalent to C#'s Action<T1, T2> delegate
 * https://msdn.microsoft.com/en-us/library/bb549311(v=vs.110).aspx
 * 
 * @author Moritz Berning
 *
 * @param <T1>
 * @param <T2>
 */
public class Action2<T1, T2> {

	private List<ActionElement2<T1, T2>> methods = new ArrayList<ActionElement2<T1, T2>>();

	/**
	 * Equivalent to C# Action's + operator
	 * 
	 * @param method:
	 *            method with matching signature
	 */
	public void add(ActionElement2<T1, T2> method) {
		methods.add(method);
	}

	/**
	 * Equivalent to C# Action's - operator
	 * 
	 * @param method:
	 *            method with matching signature
	 */
	public void remove(ActionElement2<T1, T2> method) {
		methods.remove(method);
	}

	/**
	 * Equivalent to C# Action's = operator
	 * 
	 * @param method:
	 *            method with matching signature
	 */
	public void set(ActionElement2<T1, T2> method) {
		methods = new ArrayList<ActionElement2<T1, T2>>();
		methods.add(method);
	}

	/**
	 * Equivalent to C# Action's method call
	 * 
	 * @param argument:
	 *            argument of matching type
	 */
	public void call(T1 argument1, T2 argument2) {
		for (ActionElement2<T1, T2> method : methods) {
			method.call(argument1, argument2);
		}
	}
}
