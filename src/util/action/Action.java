package util.action;

import java.util.ArrayList;
import java.util.List;

/**
 * Equivalent to C#'s Action delegate
 * https://msdn.microsoft.com/en-us/library/018hxwa8(v=vs.110).aspx
 * 
 * @author Moritz Berning
 *
 * @param <T>
 */
public class Action<T> {

	private List<ActionElement<T>> methods = new ArrayList<ActionElement<T>>();

	/**
	 * Equivalent to C# Action's + operator
	 * 
	 * @param method:
	 *            method with matching signature
	 */
	public void add(ActionElement<T> method) {
		methods.add(method);
	}

	/**
	 * Equivalent to C# Action's - operator
	 * 
	 * @param method:
	 *            method with matching signature
	 */
	public void remove(ActionElement<T> method) {
		methods.remove(method);
	}

	/**
	 * Equivalent to C# Action's = operator
	 * 
	 * @param method:
	 *            method with matching signature
	 */
	public void set(ActionElement<T> method) {
		methods = new ArrayList<ActionElement<T>>();
		methods.add(method);
	}

	/**
	 * Equivalent to C# Action's method call
	 * 
	 * @param argument:
	 *            argument of matching type
	 */
	public void call(T argument) {
		for (ActionElement<T> method : methods) {
			method.call(argument);
		}
	}
}
