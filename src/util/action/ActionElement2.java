package util.action;

@FunctionalInterface
public interface ActionElement2<T1, T2> {
	void call(T1 p1, T2 p2);
}
