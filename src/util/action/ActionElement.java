package util.action;

@FunctionalInterface
public interface ActionElement<T> {
	void call(T s);
}
