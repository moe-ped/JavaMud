package util.action;

import java.util.ArrayList;
import java.util.List;

/**
 * Equivalent to C#'s Action<T1, T2, T3> delegate
 * https://msdn.microsoft.com/en-us/library/bb549392(v=vs.110).aspx
 * 
 * @author Moritz Berning
 *
 * @param <T1>
 * @param <T2>
 * @param <T3>
 */
public class Action3<T1, T2, T3> {

	private List<ActionElement3<T1, T2, T3>> methods = new ArrayList<ActionElement3<T1, T2, T3>>();

	/**
	 * Equivalent to C# Action's + operator
	 * 
	 * @param method:
	 *            method with matching signature
	 */
	public void add(ActionElement3<T1, T2, T3> method) {
		methods.add(method);
	}

	/**
	 * Equivalent to C# Action's - operator
	 * 
	 * @param method:
	 *            method with matching signature
	 */
	public void remove(ActionElement3<T1, T2, T3> method) {
		methods.remove(method);
	}

	/**
	 * Equivalent to C# Action's = operator
	 * 
	 * @param method:
	 *            method with matching signature
	 */
	public void set(ActionElement3<T1, T2, T3> method) {
		methods = new ArrayList<ActionElement3<T1, T2, T3>>();
		methods.add(method);
	}

	/**
	 * Equivalent to C# Action's method call
	 * 
	 * @param argument:
	 *            argument of matching type
	 */
	public void call(T1 argument1, T2 argument2, T3 argument3) {
		for (ActionElement3<T1, T2, T3> method : methods) {
			method.call(argument1, argument2, argument3);
		}
	}
}
