package util;

public class StringUtils {

	public static String capitalize(String original) {
		return original.substring(0, 1).toUpperCase() + original.substring(1, original.length());
	}

}
