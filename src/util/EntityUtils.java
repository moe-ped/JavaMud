package util;

import model.Entity;
import model.component.Name;
import model.component.Type;

public class EntityUtils {

	public static String getFullName(Entity entity) {
		StringBuilder sb = new StringBuilder();
		Name nameComponent = entity.getComponent(Name.class);
		if (nameComponent != null) {
			String name = nameComponent.getName();
			name = StringUtils.capitalize(name);
			sb.append(name);
			sb.append(", the");
		} else if (nameComponent == null) {
			sb.append("a");
		}
		sb.append(" ");
		Type typeComponent = entity.getComponent(Type.class);
		if (typeComponent == null) {
			sb.append("strange thing");
		} else {
			sb.append(typeComponent.getType());
		}
		return sb.toString();
	}

	public static String getSimpleName(Entity entity) {
		Name nameComponent = entity.getComponent(Name.class);
		if (nameComponent != null) {
			return StringUtils.capitalize(nameComponent.getName());
		}
		Type typeComponent = entity.getComponent(Type.class);
		if (typeComponent != null) {
			return typeComponent.getType();
		}
		return "strange thing";
	}

}
